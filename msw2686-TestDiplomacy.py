
from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_solver


class TestDiplomacy (TestCase):

    def test_solve(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solver(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]")

    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB Austin Support A\nC Dallas Support B\nD Denver Support C")
        w = StringIO()
        diplomacy_solver(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Austin\nC Dallas\nD Denver")

    def test_solve3(self):
        r = StringIO("A Madrid Hold\nB Austin Move Madrid\nC Dallas Move Austin\nD Denver Move Madrid")
        w = StringIO()
        diplomacy_solver(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]")

    def test_solve4(self):
        r = StringIO("A Madrid Hold\nB Dallas Support A\nC Paris Support A\nD Denver Move Madrid")
        w = StringIO()
        diplomacy_solver(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Dallas\nC Paris\nD [dead]")


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
....
----------------------------------------------------------------------
Ran 4 tests in 0.001s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
....
----------------------------------------------------------------------
Ran 4 tests in 0.001s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          60      3     42      3    94%   38->39, 39, 46->42, 57->58, 58-59
TestDiplomacy.py      26      0      0      0   100%
--------------------------------------------------------------
TOTAL                 86      3     42      3    95%
"""